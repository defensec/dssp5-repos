Summary: dssp5 rpm package repository
Name: dssp5-repos
Version: 0.1
Release: 1
License: Unlicense
Group: System Environment/Base
Source: dssp5-repos.tgz
URL: https://github.com/DefenSec/dssp5-repos
BuildArch: noarch

%description
dssp5 rpm package repository files for dnf along with gpg public keys

%prep
%autosetup -n dssp5-repos

%build

%install
install -d -m 755 %{buildroot}/etc/pki/rpm-gpg
install -m 644 RPM-GPG-KEY-DSSP5 %{buildroot}/etc/pki/rpm-gpg/
install -d -m 755 %{buildroot}/etc/yum.repos.d
install -m 644 dssp5.repo %{buildroot}/etc/yum.repos.d/

%files
%defattr(-,root,root,-)
%dir /etc/yum.repos.d
%config(noreplace) /etc/yum.repos.d/dssp5.repo
%dir /etc/pki/rpm-gpg
/etc/pki/rpm-gpg/RPM-GPG-KEY-DSSP5

%changelog
* Sat Apr 24 2021 Dominick Grift <dominick.grift@defensec.nl> - 0.1-1
- Initial package

